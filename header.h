/* * *
 *
 * E. Shehadi 2016-01-04 (final FMM3D version)
 *
 * header.h is a header file containing all declarations of functions needed to
 * start-up an FMM algorithm in 3D via spherical harmonics.
 *
 * This code follows closely and is a direct implementation of the FMM algorithm
 * as described in the following paper:
 *
 * "A Short Cource on Fast Multipole Methods" by and Greengard.                 *
 *                                                                              *
 * **************************************************************************** */
#ifndef __header_h
#define __header_h

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <complex.h>

double factorial(double arg);
double permFunc_A(int nn1,int mm1);
double _Complex iSign(int arg1, int arg2);
double Plegendre(int deg, int ord, double u);
double cart2spher(double *from, double *to, int coord);
double _Complex harmonic(int degree, int order, double zenith, double azimuth);

double _Complex *shift_m2m(int p,
                        double rho, double alpha, double beta,
                        double _Complex *Coef);

double _Complex *shift_m2l(int p,
                          double rho, double alpha, double beta,
                          double _Complex *Coef);

double _Complex *shift_l2l(int p,
                          double rho, double alpha, double beta,
                          double _Complex *Coef);

double _Complex potential(int p, int option,
                          double rad, double theta, double phi,
                          double _Complex *Coef);

double _Complex *multipole_expand(int p, int total, double *charge,
                                  double *rho, double *alpha, double *beta);


const int Pmax = 30;
const int Nparticles = 1;
double _Complex pot, pot0, err;
double _Complex *ptr;
double _Complex *ME;
double _Complex *M2M;
double _Complex *M2L;
double _Complex *L2L;

#endif /* __header_h */
