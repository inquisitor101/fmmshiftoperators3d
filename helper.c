/* * *
 *
 * E. Shehadi 2016-01-04 (final FMM3D version)
 *
 * helper.c is a script containing helper functions required for calculating
 * the expansion and shift transitions for a FMM algorithm in 3D.
 *
 * This code follows closely and is a direct implementation of the FMM algorithm
 * as described in the following paper:
 *
 * "A Short Cource on Fast Multipole Methods" by and Greengard.                 *
 *                                                                              *
 * **************************************************************************** */
#include "header.h"

void setUsFree()
{
  /* This function deallocates any dynamically allocated date before exiting.
   ************************************************************************** */
  free(ME);
  free(M2M);
  free(M2L);
  free(L2L);
}

double _Complex iSign(int arg1, int arg2)
{
  /* This is a helper function that assists in expansion translation calculation.
   * It takes two inputs and returns the following expression evaluated:
   *
   *
   * iSign(a, b) = i^( |a| - |b| - |a-b| )
   *
   ****************************************************************************** */
  int exprn = abs(arg1) - abs(arg2) - abs(arg1 - arg2);
  return cpow(I, exprn);
}

double permFunc_A(int nn1, int mm1)
{
  /* This is a helper function that assists in expansion translation calculation.
   * It takes two inputs and returns the A_n^m expression below:
   *
   *                             (-1)^n
   * A_n^m =      -----------------------------------
   *               sqrt( (nn1 - mm1)! * (nn1 + mm1)! )
   *
   * For more information read: "A Short Cource on Fast Multipole Methods" by
   * Beatson and Greengard. Specifically equation (5.23)
   ***************************************************************************** */
  double perm11, perm22;
  perm11 = factorial(nn1-mm1);
  perm22 = factorial(nn1+mm1);
  if (nn1%2==0){
    return 1.0/sqrt(perm11*perm22);
  }
  else {
    return -1.0/sqrt(perm11*perm22);
  }
}

double cart2spher(double *from, double *to, int coord)
{
  /* This is a cartesian to spherical transformation function. It transformes
   * vector v(x, y, z) = (x1-x0, y1-y0, z1-z0) into v(r, \theta, \phi)
   *
   * where (x1, y1, z1) = (to[0], to[1], to[2])
   *       (x0, y0, z0) = (from[0], from[1], from[2])
   *************************************************************************** */
  double spherical[3];
	double xR, yR, zR;
	xR = to[0] - from[0];
	yR = to[1] - from[1];
	zR = to[2] - from[2];
	spherical[0] = sqrt( (xR*xR) + (yR*yR) + (zR*zR) );
  spherical[1] = acos(zR/spherical[0]);
  spherical[2] = atan2(yR,xR);
  switch(coord) {
    case 1:  //  radius
      return spherical[0];
      break;
    case 2:  //  theta: zenith angle
      return spherical[1];
      break;
    case 3:  //  phi  : azimuth angle
      return spherical[2];
      break;
    default: //  something is wrong !?
      return printf("Wrong input in cart2spher function");
      break;
  }
}


double factorial(double arg)
{
  /* This function solves recursively for an input factorial.
   ********************************************************** */
  if (arg == 0 || arg == 1)
    return 1.0;
  else {
    if (arg == 7)
      return 5040.0;
    if (arg == 6)
      return 720.0;
    if (arg == 5)
      return 120.0;
    if (arg == 4)
      return 24.0;
    if (arg == 3)
      return 6.0;
    if (arg == 2)
      return 2.0;
    else {
      return arg*factorial(arg-1);
    }
  }
}

double Plegendre(int deg, int ord, double u)
{
  /* This is an associated Legendre polynomial function. It takes as input the
   * degree and order along with the cosine of the angle and returns its
   * corresponding associated Legendre polynomial.
   *
   * The commented sections are for extra features that might by useful for
   * later practices. They are excluded on purpose for now as in compliance
   * with Greengard's paper. Read highlighted comments for details.
   **************************************************************************** */
  int alt = 0;
  //NOTE: commented sections are legendre polynomial negative order cases which
  //      are excluded on purpose in accordance with the referenced paper.
  /*if (ord<0){
    alt=1;
  }*/
  ord = abs(ord);
  int num, sign, i;
  double pmm, var1, fact, pm1m, pnm;
  if (fabs(u) > 1.0){
    printf("Error! Legendre argument input is greater than one.");
    exit(0);
  }
  if (ord > deg){
    return 0;
  }
  pmm = 1.0;
  if (ord > 0){
    var1 = sqrt(1.0 - u*u);
    fact = 1.0;
    for (i=0; i<ord; i++){
      pmm *= var1*fact;
      fact += 2.0;
    }
    sign = (ord%2==0) ? 1 : -1;
    pmm = sign*pmm;
  }
  if (deg == ord){
    if (alt==0){
      return pmm;
    }
    else{
      return sign*factorial(deg-ord)*pmm/factorial(deg+ord);
    }
  }
  else {
    pm1m = u*(2*ord + 1.0)*pmm;
    if (deg == (ord+1) ){
      if (alt==0){
        return pm1m;
      }
      else {
        return sign*factorial(deg-ord)*pm1m/factorial(deg+ord);
      }
    }
    else {
      for (num=ord+2; num<=deg; num++){
        pnm = ( u*(2*num-1.0)*pm1m - (num+ord-1.0)*pmm )/(num-ord);
        pmm = pm1m;
        pm1m = pnm;
      }
      if (alt==0){
        return pnm;
      }
      else {
        return sign*factorial(deg-ord)*pnm/factorial(deg+ord);
      }
    }
  }
}


double _Complex harmonic(int degree, int order, double zenith, double azimuth)
{
  /* This is a spherical harmonics function. It takes as input the degree, order,
   * zenith angle and azimuth angle and returns the corresponding spherical
   * harmonic value in complex.
   *
   * The commented parts in this function are so on purpose and in accordance
   * with Greengard's assumption on excluding the normalization factors.
   *
   * For more information read: "The Rapid Evaluation of Potential Fields in
   * Particle Systems" by Greengard. Specifically chapter 3 and section 3.3 which
   * is devoted to explaining the theory behind spherical harmonics in detail.
   ****************************************************************************** */
  double _Complex expression = 0.0 + 0.0*I;
  double angle = cos(zenith);
  expression = sqrt( factorial(degree-abs(order)) / factorial(degree+abs(order)) )*Plegendre(degree, abs(order), angle)*cexp(order*azimuth*I);

  //NOTE: the commented parts are normalization factors which are excluded
  //      on purpose in accordance to the refernced paper.
  return expression;//*sqrt((2*degree+1)/(4*M_PI));
}
