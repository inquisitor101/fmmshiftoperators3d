# README #

This is a C-based code for the implementation of the classic **Fast Multipole Method (FMM)** shift operators in three dimensions. These translation operators are: _local-to-local_, _multipole-to-local_ and _multipole-to-multipole_. Included in this repository is a makefile for testing purposes -- you can discard it, if you plan to optimize or further develop this basic code version.

To compile (on a Linux-based OS), use:  **make fmm**

To execute, use:  **./fmm <p\>**     OR   **./fmm**

The first command returns only the results given a single p value, while the second does sequentially all p values from 1 to 20 (inclusive). Here, **p** stands for the expansion polynomial order.
  
 
 
# NOTE
When using this code, please make sure you read the license file attached in it.

# Reference
This code is based on the original work of L. Greengard and V. Rokhlin presented in [A fast algorithm for particle simulations](https://doi.org/10.1016/0021-9991(87)90140-9). Moreover, this code follows closely the consice lectures as provided by:

Beatson, R., & Greengard, L. (1997). A short course on fast multipole methods. Wavelets, multilevel methods and elliptic PDEs, 1, 1-37.