/* * *
 *
 * E. Shehadi 2016-01-04 (final FMM3D version)
 *
 * test.c is a script file containing all necessary data for running a testrun
 * composed of 1000 target points, one source point, an expansion center and
 * three different shift operations dependant on one another before evaluating.
 *
 * This code follows closely and is a direct implementation of the FMM algorithm
 * as described in the following paper:
 *
 * "A Short Cource on Fast Multipole Methods" by and Greengard.                 *
 *                                                                              *
 * **************************************************************************** */
 #include "shift.c"

/* configuration set-up */
int i, N =1000;
double P1[3], P2[3], P3[3], P4[3];
double a, r, source[3], mytarget[3], weight;
double Tx[1000], Ty[1000], Tz[1000], avgE0, avgE1, avgE2, avgE3;

void expandME(double *source, double *about, double weight, int p)
{
  /* multipole expansion coefficients of 'source' at 'about' are
   * computed. The return value is a set of all the truncated
   * terms of a given p value associated to this multipole moment.
   * ************************************************************* */
  int i,j;
  double center[3];
  double q[1];
  q[0] = weight;
  center[0] = cart2spher(about, source, 1);
  center[1] = cart2spher(about, source, 2);
  center[2] = cart2spher(about, source, 3);
  ptr = multipole_expand(p, Nparticles, &q[0], &center[0], &center[1], &center[2]);
  for (i=0; i<=p; i++){
    for (j=-i; j<=i; j++){
      ME[(2*p+1)*i + j+i] = *(ptr + (2*p+1)*i + j+i);
    }
  }
  free(ptr);
}

void Papprx(int field, double *ExpCenter, double *target, double _Complex *coefficients, int p)
{
  /* potential approximation is evaluated at 'target' from the multipole
   * expansion coefficients at 'ExpCenter'.
   * ******************************************************************* */
  double vec[3];
  vec[0] = cart2spher(ExpCenter,target, 1);
  vec[1] = cart2spher(ExpCenter,target, 2);
  vec[2] = cart2spher(ExpCenter,target, 3);
  pot0 = potential(p, field, vec[0], vec[1], vec[2], coefficients);
}

void shiftM2M(double *pt_1, double *pt_2, double _Complex *coef, int p)
{
  /* multipole expansion coefficients of 'pt_2' are shifted to 'pt_1'
   * The return value is a set of all the truncated
   * terms of a given p value associated to this set of multipole
   * expansion coefficients.
   * ************************************************************* */
  int i,j;
  double vector[3];
  vector[0] = cart2spher(pt_1, pt_2, 1);
  vector[1] = cart2spher(pt_1, pt_2, 2);
  vector[2] = cart2spher(pt_1, pt_2, 3);
  ptr = shift_m2m(p, vector[0], vector[1], vector[2], coef);
  for (i=0; i<=p; i++){
    for (j=-i; j<=i; j++){
      M2M[(2*p+1)*i + j+i] = *(ptr + (2*p+1)*i + j+i);
    }
  }
  free(ptr);
}

void Papprx_M2M(int field, double *pt_2, double *target, double _Complex *coefficients, int p)
{
  /* potential approximation is evaluated at 'target' from the multipole
   * expansion coefficients at 'pt_2'.
   * ******************************************************************* */
  double vec[3];
  vec[0] = cart2spher(pt_2, target, 1);
  vec[1] = cart2spher(pt_2, target, 2);
  vec[2] = cart2spher(pt_2, target, 3);
  pot0 = potential(p, field, vec[0], vec[1], vec[2], coefficients);
}

void shiftM2L(double *pt_2, double *pt_3, double _Complex *coef, int p)
{
  /* multipole expansion coefficients of 'pt_3' are converted to
   * local expansion coefficients at 'pt_2'. The return value is
   * a set of all the truncated terms of a given p value associated
   * to this set of local expansion coefficients.
   * ************************************************************* */
  int i,j;
  double vector[3];
  vector[0] = cart2spher(pt_2, pt_3, 1);
  vector[1] = cart2spher(pt_2, pt_3, 2);
  vector[2] = cart2spher(pt_2, pt_3, 3);
  ptr = shift_m2l(p, vector[0], vector[1], vector[2], coef);
  for (i=0; i<=p; i++){
    for (j=-i; j<=i; j++){
      M2L[(2*p+1)*i + j+i] = *(ptr + (2*p+1)*i + j+i);
    }
  }
  free(ptr);
}

void Papprx_M2L(int field, double *pt_3, double *target, double _Complex *coefficients, int p)
{
  /* potential approximation is evaluated at 'target' from the local
   * expansion coefficients at 'pt_3'.
   * ******************************************************************* */
  double vec[3];
  vec[0] = cart2spher(pt_3, target, 1);
  vec[1] = cart2spher(pt_3, target, 2);
  vec[2] = cart2spher(pt_3, target, 3);
  pot0 = potential(p, field, vec[0], vec[1], vec[2], coefficients);
}

void shiftL2L(double *pt_3, double *pt_4, double _Complex *coef, int p)
{
  /* local expansion coefficients of 'pt_3' are shifted to 'pt_4'.
   * The return value is a set of all the truncated terms of a given
   * p value associated to this set of local expansion coefficients.
   * *************************************************************** */
  int i,j;
  double vector[3];
  vector[0] = cart2spher(pt_4, pt_3, 1);
  vector[1] = cart2spher(pt_4, pt_3, 2);
  vector[2] = cart2spher(pt_4, pt_3, 3);
  ptr = shift_l2l(p, vector[0], vector[1], vector[2], coef);
  for (i=0; i<=p; i++){
    for (j=-i; j<=i; j++){
      L2L[(2*p+1)*i + j+i] = *(ptr + (2*p+1)*i + j+i);
    }
  }
  free(ptr);
}

void Papprx_L2L(int field, double *pt_4, double *target, double _Complex *coefficients, int p)
{
  /* potential approximation is evaluated at 'target' from the local
   * expansion coefficients at 'pt_4'.
   * ******************************************************************* */
  double vec[3];
  vec[0] = cart2spher(pt_4, target, 1);
  vec[1] = cart2spher(pt_4, target, 2);
  vec[2] = cart2spher(pt_4, target, 3);
  pot0 = potential(p, field, vec[0], vec[1], vec[2], coefficients);
}

void Pexact(double *eval_this, double *eval_at)
{
  /* exact potential calculation is computed via direct method at target
   * point: 'eval_at' due to source point: 'eval_this'.
   * ******************************************************************* */
   pot = 1.0/cart2spher(eval_this, eval_at, 1);
}

void error(int p)
{
  /* difference error between the direct method and approximate method
   * is calculated and returned in absolute value.
   * ***************************************************************** */
  err = fabs(creal(pot0) - creal(pot));
}


void initializeData()
{
  /* initialize and assign all required data points to start the test-run.
   * ********************************************************************* */
  weight = 1.0;
  source[0] = 1.5, source[1] = 1.2, source[2] = 1.2;
  P1[0] = 3.2, P1[1] = 4.9, P1[2] = 6.3;
  P2[0] = 8.2, P2[1] = 8.33, P2[2] = 8.9;
  P3[0] = 90.1, P3[1] = 92.2, P3[2] = 97.0;
  P4[0] = 96.99, P4[1] = 95.4, P4[2] = 91.1;

  for (i=0; i<N; i++){
    Tx[i] = 90.0+10.0*drand48();
    Ty[i] = 90.0+10.0*drand48();
    Tz[i] = 90.0+10.0*drand48();
  }
}

void tryme()
{
  /* calculate all evaluations from p=1 up to p=20 and report back the
   * difference error in each test.
   * ***************************************************************** */
  int p;
  for (p=1; p<20+1; p++){
    expandME(source, P1, weight, p);          // equation 5.16
    shiftM2M(P2, P1, ME, p);                  // equation 5.22
    shiftM2L(P3, P2, M2M, p);                 // equation 5.26
    shiftL2L(P3, P4, M2L, p);                 // equation 5.30
    avgE0 = 0.0, avgE1 = 0.0, avgE2 = 0.0, avgE3 = 0.0;
    for (i=0; i<N; i++){
      mytarget[0] = Tx[i];
      mytarget[1] = Ty[i];
      mytarget[2] = Tz[i];
      Pexact(source, mytarget);
      Papprx(1, P1, mytarget, ME, p);           // equation 5.15
      error(p);
      avgE0 += creal(err)/N;

      Papprx_M2M(1, P2, mytarget, M2M, p);      // equation 5.20
      error(p);
      avgE1 += creal(err)/N;

      Papprx_M2L(0, P3, mytarget, M2L, p);      // equation 5.25
      error(p);
      avgE2 += creal(err)/N;

      Papprx_L2L(0, P4, mytarget, L2L, p);      // equation 5.28
      error(p);
      avgE3 += creal(err)/N;
    }
    printf("\n                                      *");
    printf("\n* * * * * * * * Test #%d * * * * * * * *", p);
    printf("\n* p = %d                         ", p);
    printf("\n* %e using ME", avgE0);
    printf("\n  %e using ME+M2M", avgE1);
    printf("\n  %e using ME+M2M+M2L", avgE2);
    printf("\n  %e using ME+M2M+M2L+L2L   *", avgE3);

  }
}
