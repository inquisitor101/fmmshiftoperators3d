/* * *
 *
 * E. Shehadi 2016-01-04 (final FMM3D version)
 *
 * shift.c is a script containing all translation and expansion operators
 * required to fully implement a classical FMM algorithm in 3D via spherical
 * harmonics.
 *
 * This code follows closely and is a direct implementation of the FMM algorithm
 * as described in the following paper:
 *
 * "A Short Cource on Fast Multipole Methods" by and Greengard.                 *
 *                                                                              *
 * **************************************************************************** */
#include "helper.c"


double _Complex *multipole_expand(int p, int total, double *charge,
                                  double *rho, double *alpha, double *beta)
{
  /* This is a multipole expansion that uses spherical harmonics and returns
   * a whole set of coefficients depending on truncation value and point
   * arguments input. A description of the input arguments is below:
   *
   * p     :  truncation value
   * total :  total number of sources/particles contributing to expansion.
   * charge:  charge or weight of each source particle.
   * Q_i(rho, alpha, beta) particles that are expanded at M(Xto, Yto, Zto).
   *
   * For more information read: "A Short Cource on Fast Multipole Methods" by
   * Beatson and Greengard.
   *
   * For a more in-depth investigation, check: "The Rapid Evaluation of
   * Potential Fields in Particle Systems" by Greengard.
   * ************************************************************************ */
  int n, m, particle;
  double mult = 1.0;
  double _Complex *expansion = (double _Complex*)calloc((p+1)*(2*p+1), sizeof(double _Complex));
  if (expansion==NULL){
    printf("Error! memory not allocated.");
    exit(0);
  }
  for (particle=0; particle<total; particle++){
    for (n=0; n<=p; n++){
      mult = (n > 0) ? mult*rho[particle] : 1.0;
      for (m=-n; m<=n; m++){
        expansion[(2*p+1)*n + m+n] += charge[particle]*mult\
        *(harmonic(n, -m, alpha[particle], beta[particle]));
      }
    }
  }
  return expansion;
}


double _Complex *shift_m2m(int p,
                        double rho, double alpha, double beta,
                        double _Complex *Coef)
{
  /* This is a multipole-to-multipole shift operation. It takes the current
   * expansion coefficients and returns a modified set of coefficients
   * depending on truncation value and point arguments input. A description
   * of the input arguments is below:
   *
   * p:  truncation value
   * (rho, alpha, beta) with expansion coefficients 'Coef' that needs
   * shifting via M2M.
   *
   * For more information read: "A Short Cource on Fast Multipole Methods" by
   * Beatson and Greengard.
   *
   * For a more in-depth investigation, check: "The Rapid Evaluation of
   * Potential Fields in Particle Systems" by Greengard.
   * ************************************************************************* */
  int j, k, n, m;
  double mult;
  double _Complex *translation1 = (double _Complex*)calloc((p+1)*(2*p+1), sizeof(double _Complex));
  if (translation1==NULL){
    printf("Error! memory not allocated.");
    exit(0);
  }
  mult = 1.0;
  for (j=0; j<=p; j++){
    for (k=-j; k<=j; k++){
      for (n=0; n<=j; n++){
        mult = (n > 0) ? mult*rho : 1.0;
        for (m=-n; m<=n; m++){
          if ( abs(k-m) <= (j-n) ){//CHANGED: works without iSign, why!?
            translation1[(2*p+1)*j + k+j] += Coef[(2*p+1)*(j-n) + k+j-m-n]*iSign(k,m)\
            *mult*( harmonic(n, -m, alpha, beta) )*permFunc_A(n,m)\
            *permFunc_A(j-n,k-m)/permFunc_A(j,k);
          }
        }
      }
    }
  }
  return translation1;
}

double _Complex *shift_m2l(int p,
                      double rho, double alpha, double beta,
                      double _Complex *Coef)
{
  /* This is a multipole-to-local shift operation. It takes the current
   * expansion coefficients and returns a modified set of coefficients
   * depending on truncation value and point arguments input. A description
   * of the input arguments is below:
   *
   * p:  truncation value
   * (rho, alpha, beta) with expansion coefficients 'Coef' that needs
   * shifting via M2L.
   *
   * For more information read: "A Short Cource on Fast Multipole Methods" by
   * Beatson and Greengard.
   * For a more in-depth investigation, check: "The Rapid Evaluation of
   * Potential Fields in Particle Systems" by Greengard.
   * ************************************************************************ */
  int sgn, j, k, n, m;
  double mult, mult2;
  double _Complex *translation2 = (double _Complex*)calloc((p+1)*(2*p+1), sizeof(double _Complex));
  if (translation2==NULL){
    printf("Error! memory not allocated.");
    exit(0);
  }
  mult  = 1.0;
  mult2 = 1.0;
  for (j=0; j<=p; j++){
    mult2 *= rho;
    for (k=-j; k<=j; k++){
      for (n=0; n<=p; n++){
        mult = (n > 0) ? mult*rho : mult2;
        for (m=-n; m<=n; m++){
          sgn = (n%2 == 0) ? 1 : -1;
          translation2[(2*p+1)*j + k+j] += sgn*iSign(k-m, k)*Coef[(2*p+1)*n + m+n]\
          *permFunc_A(n, m)*permFunc_A(j, k)*harmonic(j+n, m-k, alpha, beta)\
          /(permFunc_A(j+n, m-k)*mult);
        }
      }
    }
  }
  return translation2;
}

double _Complex *shift_l2l(int p,
                      double rho, double alpha, double beta,
                      double _Complex *Coef)
{
  /* This is a local-to-local shift operation. It takes the current
   * expansion coefficients and returns a modified set of coefficients
   * depending on truncation value and point arguments input. A description
   * of the input arguments is below:
   *
   * p:  truncation value
   * (rho, alpha, beta) with expansion coefficients 'Coef' that needs
   * shifting via L2L.
   *
   * For more information read: "A Short Cource on Fast Multipole Methods" by
   * Beatson and Greengard.
   *
   * For a more in-depth investigation, check: "The Rapid Evaluation of
   * Potential Fields in Particle Systems" by Greengard.
   * ************************************************************************ */
  int sgn, j, k, n, m;
  double mult;
  double _Complex *translation3 = (double _Complex*)calloc((p+1)*(2*p+1), sizeof(double _Complex));
  if (translation3==NULL){
    printf("Error! memory not allocated.");
    exit(0);
  }
  for (j=0; j<=p; j++){
    mult = (j > 0) ? mult/rho : 1.0;
    for (k=-j; k<=j; k++){
      for (n=j; n<=p; n++){
        mult = (n > j) ? mult*rho : 1.0;
        for (m=-n; m<=n; m++){
          sgn = ((n+j)%2 == 0) ? 1 : -1;
          if ( abs(m-k) <= (n-j) ){
            translation3[(2*p+1)*j + k+j] += sgn*iSign(m, k)*Coef[(2*p+1)*n + m+n]\
            *permFunc_A(n-j, m-k)*permFunc_A(j, k)*mult\
            *harmonic(n-j, m-k, alpha, beta)/permFunc_A(n, m);
          }
        }
      }
    }
  }
  return translation3;
}

double _Complex potential(int p, int option,
                    double rad, double theta, double phi,
                    double _Complex *Coef)
{
  /* This is a potential evaluation. It can be for both: near and far-field.
   * It takes the current expansion terms and evaluates them with respect to
   * a target point depending on truncation and point arguments input. A
   * description of the input arguments is below:
   *
   * p     :  truncation value
   * option:  0 for near-field, 1 for far-field evaluation
   * (rad, theta, phi) vector from point/expansion towards target point with
   * expansion coefficients 'Coef'.
   *
   * For more information read: "A Short Cource on Fast Multipole Methods" by
   * Beatson and Greengard.
   *
   * For a more in-depth investigation, check: "The Rapid Evaluation of
   * Potential Fields in Particle Systems" by Greengard.
   * ************************************************************************ */
  int j, k;
  double mult1, mult2;
  double _Complex potFAR = 0.0 + 0.0*I;
  double _Complex potNEAR = 0.0 + 0.0*I;

  switch(option){
    case 0:
      potNEAR = Coef[(2*p+1)*0 + 0+0]*harmonic(0, 0, theta, phi);
      mult1 = 1.0;
      for (j=1; j<=p; j++){
        mult1 *= rad;
        for (k=-j; k<=j; k++){
          potNEAR += Coef[(2*p+1)*j + k+j]*harmonic(j, k, theta, phi)*mult1;
        }
      }
      return potNEAR;
      break;
    case 1:
    potFAR = Coef[(2*p+1)*0 + 0+0]*harmonic(0, 0, theta, phi)/rad;
    mult2 = rad;
      for (j=1; j<=p; j++){
        mult2 *= rad;
        for (k=-j; k<=j; k++){
          potFAR += Coef[(2*p+1)*j + k+j]*harmonic(j, k, theta, phi)/mult2;
        }
      }
      return potFAR;
      break;
    default:
      return printf("wrong input in potential function !");
      break;
  }
}
