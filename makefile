########################################################
# Makefile for driver.c
# Author: E. Shehadi
#
# Advanced Topics in Scientific Computing II: FMM in 3D
#
########################################################
CC      = gcc
CCFLAGS = -O3
LIBS    = -lm

fmm:	driver.c
	$(CC) $(CCFLAGS) -o fmm driver.c $(LIBS)
clean:
	rm -f *.o *~ fmm
