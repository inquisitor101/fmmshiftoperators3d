/* * *
 *
 * E. Shehadi 2016-01-04 (final FMM3D version)
 *
 * driver.c is a driver class script. It is responsible for running the FMM
 * algorithm by testing and validating the error via a cross-comparison
 * with the direct method.
 *
 * NB: To run this file, please refer to the Makefile
 *
 * This code follows closely and is a direct implementation of the FMM algorithm
 * as described in the following paper:
 *
 * "A Short Cource on Fast Multipole Methods" by and Greengard.                 *
 *                                                                              *
 * **************************************************************************** */
 #include "test.c"

int main(int argc, char **argv) {

  /* input truncation value */
  int p = (argv[1] == NULL) ? -1 : atoi(argv[1]);

  /* allocate all data */
  ME = (double _Complex*)calloc( (Pmax+1)*(2*Pmax+1), sizeof(double _Complex));
  M2M = (double _Complex*)calloc( (Pmax+1)*(2*Pmax+1), sizeof(double _Complex));
  M2L = (double _Complex*)calloc( (Pmax+1)*(2*Pmax+1), sizeof(double _Complex));
  L2L = (double _Complex*)calloc( (Pmax+1)*(2*Pmax+1), sizeof(double _Complex));

  /* initialize all data coordinates and shift points */
  initializeData();

  /* if no specific input, then output error from p=1 to p=20 inclusive*/
  if (p==-1) {
    tryme();
  }
  /* output specified p exclusively */
  else {

    expandME(source, P1, weight, p);          // equation 5.16
    shiftM2M(P2, P1, ME, p);                  // equation 5.22
    shiftM2L(P3, P2, M2M, p);                 // equation 5.26
    shiftL2L(P3, P4, M2L, p);                 // equation 5.30
    avgE0 = 0.0, avgE1 = 0.0, avgE2 = 0.0, avgE3 = 0.0;
    for (i=0; i<N; i++){
      mytarget[0] = Tx[i];
      mytarget[1] = Ty[i];
      mytarget[2] = Tz[i];
      Pexact(source, mytarget);
      Papprx(1, P1, mytarget, ME, p);           // equation 5.15
      error(p);
      avgE0 += creal(err)/N;

      Papprx_M2M(1, P2, mytarget, M2M, p);      // equation 5.20
      error(p);
      avgE1 += creal(err)/N;

      Papprx_M2L(0, P3, mytarget, M2L, p);      // equation 5.25
      error(p);
      avgE2 += creal(err)/N;

      Papprx_L2L(0, P4, mytarget, L2L, p);      // equation 5.28
      error(p);
      avgE3 += creal(err)/N;
      }
      printf("\np = %d   \n", p);
      printf("\nEavg0: %e", avgE0);
      printf("\nEavg1: %e", avgE1);
      printf("\nEavg2: %e", avgE2);
      printf("\nEavg3: %e", avgE3);
    }

  printf("\n\nTarget points: %d", N);
  printf("\nError: | Direct - Approximate |");

  printf("\n");
  setUsFree();
}
